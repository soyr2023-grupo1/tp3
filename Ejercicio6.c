#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/ipc.h>
#include <signal.h>
#include <string.h>

#define NUMERO1 26
#define ARCHIVO "/bin/more"
#define NUMERO2 18

#define a 4
#define b 5
unsigned int ft=0, f=0;

void handler_sigterm(int sig);
void handler_signal1(int sig);

void kill_process (char t, int * p_memoria, pid_t * p_memoria_aux, int IDshm, int IDshmaux);

int main(int argc, char** argv) {

    int * p_memoria = NULL;         // Puntero para datos cifrados
    int datos[22]={a,b,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,26}; //Claves pub y Datos sin cifrar
    pid_t * p_memoria_aux = NULL;   // Puntero para PIDs
    int i;

    //verifico las parámetros pasados por línea de comando
    if ((argc != 2) || ((strcmp(argv[1], "a") != 0) && (strcmp(argv[1], "b") != 0)))
    {
        printf("Sintaxis: %s [a|b]\n", argv[0]);
        return 1;
    }

    signal(SIGTERM,handler_sigterm);// Instalo el handler de SIGTERM
    signal(SIGUSR1,handler_signal1); // Instalo el handler de SIGUSR1

//------------------------ MEMORIA COMPARTIDA ARREGLO ------------------------
    //genero clave para la memoria compartida
    key_t Clave1 = ftok (ARCHIVO, NUMERO1);
	if (Clave1 == -1)
	{
		printf("No consegui  clave para memoria compartida\n");
		exit(1);
	}

    //reservo espacio para memoria compartida con ID shmid
    int IDshm = shmget(Clave1, 22 * sizeof(int), IPC_CREAT | 0666);//20 elementos + 2 claves
    if (IDshm == -1)
    {
        perror("shmget");
        return 1;
    }

    //asocio el id de la memoria compartida a un puntero
    p_memoria = shmat(IDshm, NULL, 0);
    if (p_memoria == NULL)
    {
        perror("shmat");
        return 1;
    }  

//-------------------- MEMORIA COMPARTIDA AUXILIAR --------------------

    //genero clave para la memoria compartida auxiliar, donde cada proceso va a almacenar su PID
    key_t Clave2 = ftok (ARCHIVO, NUMERO2);
	if (Clave2 == -1)
	{
		printf("No consegui  clave para memoria compartida auxiliar\n");
		exit(1);
	}

     //reservo espacio para memoria compartida con ID shmid
    int IDshmaux = shmget(Clave2, 2 * sizeof(pid_t), IPC_CREAT | 0666);
    if (IDshmaux == -1)
    {
        perror("shmget");
        return 1;
    }

    //asocio el id de la memoria compartida a un puntero
    p_memoria_aux = shmat(IDshmaux, NULL, 0);
    if (p_memoria_aux == NULL)
    {
        perror("shmat");
        return 1;
    }

    //Escribo las claves publicas en el memoria compartida  
        p_memoria[0]=a;  // Clave A
        p_memoria[1]=b;  // Clave B

    //limpio bloque auxiliar
    p_memoria_aux [0] = 0;
    p_memoria_aux [1] = 0;
    
//-------------------- PROCESO A --------------------
    if (strcmp(argv[1], "a") == 0)// Si es el proceso A 
    {
        system("clear");
        printf(" ===================================\n");
        printf("         EJECUTANDO PROCESO A\n");
        printf(" ===================================\n\n");
    }

//-------------------- PROCESO B --------------------
    if (strcmp(argv[1], "b") == 0)// Si es el proceso B 
    {   
        system("clear");
        printf(" ===================================\n");
        printf("         EJECUTANDO PROCESO B\n");
        printf(" ===================================\n\n");
    }

    printf ("Esperando a proceso\n");
    while ((p_memoria_aux[0] == 0) || (p_memoria_aux[1] == 0))      // Espero a que se inicie un proceso de la
    {                                                               // otra clase(A espera a B o B espera a A)
        if (strcmp(argv[1], "a") == 0)
            p_memoria_aux[0] = getpid();

        if (strcmp(argv[1], "b") == 0)
            p_memoria_aux[1] = getpid();
    }

//-------------------- BUCLE PRINCIPAL --------------------   
    while (1)
    {
        // ----- PROCESO A -----
        if (strcmp(argv[1], "a") == 0)
        {
            // CIFRADO Y ESCRITURA EN MEM. COMP. (2 a 11)
            printf("\nEscribiendo datos en memoria compartida\n");
            for (i=2;i<12;i++)// Escritura
            {
                printf("Proceso A (%d): Escribiendo dato %d\n", getpid(), datos[i]);
                p_memoria[i]= ((p_memoria[0]*datos[i])+p_memoria[1])%27;
                sleep (1);
            }
            printf("\n");// Salto de linea para separar etapas

            //ENVIAR SEÑAL A PROCESO B (PIDB = p_memoria_aux[1])
            if (kill(p_memoria_aux[1],SIGUSR1) == 0 )
                printf(" -Señal SIGUSR1 enviada a B (%d)-\n\n",p_memoria_aux[1]);
            else 
                printf("Error al enviar señal SIGUSR1\n");

            printf("- Esperando a que B termine de escribir -\n\n");
            while((f == 0) && (ft == 0)){} // Espero hasta que Pro B envie alguna señal

            // SI SE RECIBE SIGTERM
            if (ft ==1 )                           
                kill_process ('a', p_memoria, p_memoria_aux, IDshm, IDshmaux);

            f = 0;
            
            //LECTURA Y DECIFRADO DE LOS VALORES DE LA MEMORIA COMPARTIDA (12 a 21)
            printf("\n\nLeyendo datos de memoria compartida\n");
            for (i=12;i<22;i++)// Escritura
            {
                printf("Proceso A (%d): Dato leido %d\t\t",getpid(),p_memoria[i]);
                printf("Dato descifrado %d\n",(7*p_memoria[i]+19)%27);
            }

            //ENVIAR SEÑAL SIGUSR2 A PROCESO B
            kill (p_memoria_aux[1], SIGUSR1);

            //ESPERO A QUE PROCESO B TERMINE DE LEER
            while (f == 0 && ft == 0){}

            // SI SE RECIBE SIGTERM
            if (ft ==1 )
                kill_process ('a', p_memoria, p_memoria_aux, IDshm, IDshmaux);

            f=0;
        }

        // ----- PROCESO B -----
        if (strcmp(argv[1], "b") == 0)
        {
            // CIFRADO Y ESCRITURA EN MEM. COMP. (12 a 21)
            printf("\n\nEscribiendo datos en memoria compartida\n");
            for (i=12;i<22;i++)
            {
                printf("Proceso B (%d): Escribiendo dato %d\n",getpid(),datos[i]);
                p_memoria[i]= ((p_memoria[0]*datos[i])+p_memoria[1])%27; //Cifrado y escritura en mem. comp.
                sleep (1);
            }
            printf("\n");// Salto de linea para separar etapas

            //ENVIAR SEÑAL A PROCESO A (PIDA = p_memoria_aux[0])
            if (kill(p_memoria_aux[0], SIGUSR1) == 0 )
                printf(" -Señal SIGUSR1 enviada a A (%d)-\n\n",p_memoria_aux[0]);
            else 
                printf("Error al enviar señal SIGUSR1\n");

            printf("- Esperando a que A termine de escribir -\n\n");
            while(f == 0 && ft == 0){} //esperando a que proceso A envíe la señal de que terminó de escribir

            if (ft == 1)
                kill_process ('b', p_memoria, p_memoria_aux, IDshm, IDshmaux);

            f = 0;

            //LECTURA Y DECIFRADO DE LOS VALORES DE LA MEMORIA COMPARTIDA (2 HASTA EL 11)
            printf("\nLeyendo datos de memoria compartida\n");
            for (i=2;i<12;i++)// Escritura
            {
                printf("Proceso B (%d): Dato leido %d\t\t",getpid(),p_memoria[i]);
                printf("Dato descifrado %d\n",(7*p_memoria[i]+19)%27);
            }


            //ENVIAR SEÑAL SIGUSR2 A PROCESO A
            kill (p_memoria_aux[0], SIGUSR1);

            //ESPERO A QUE PROCESO A TERMINE DE LEER
            while (f == 0 && ft == 0){}

            if (ft == 1)
                kill_process ('b', p_memoria, p_memoria_aux, IDshm, IDshmaux);

            f=0;
        }
    }
}

void handler_sigterm(int sig)
{
    ft=1; // Flag cambia a 1 si se recibe señal de terminar proceso
    printf("\n -Señal SIGTERM Recibida-\n\n");
}

void handler_signal1(int sig)
{
    f=1;  // Flag cambia a 1 si se recibe señal de que el otro proceso terminó de escribir
    printf("\n -Señal SIGUSR1 Recibida-\n\n");
}

void kill_process (char t, int * p_memoria, pid_t * p_memoria_aux, int IDshm, int IDshmaux)
{
    if (t == 'a')
        kill(p_memoria_aux[1],SIGTERM);

    //LIBERO RECURSOS
    printf ("Liberando recursos\n");
    shmdt ((const void *) p_memoria);
    shmdt ((const void *) p_memoria_aux);

    if (t == 'a')
    {
        shmctl (IDshm, IPC_RMID, (struct shmid_ds *)NULL);
        shmctl (IDshmaux, IPC_RMID, (struct shmid_ds *)NULL);
    }

    printf ("Finalizando proceso\n"); 
    exit (0);
}